atomicwrites==1.2.1
attrs==18.2.0
certifi==2018.11.29
chardet==3.0.4
Click==7.0
filelock==3.0.10
Flask==1.0.2
Flask-Cors==3.0.7
Flask-SQLAlchemy==2.3.2
idna==2.8
itsdangerous==1.1.0
Jinja2==2.10
MarkupSafe==1.1.0
more-itertools==5.0.0
pluggy==0.8.0
py==1.7.0
PyMySQL==0.9.3
requests==2.21.0
six==1.12.0
SQLAlchemy==1.2.15
toml==0.10.0
tox==3.7.0
urllib3==1.24.1
virtualenv==16.2.0
Werkzeug==0.14.1
