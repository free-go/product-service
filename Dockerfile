FROM python:3.7-alpine

WORKDIR /usr/src/

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY src/app/ ./app
COPY src/app.py .
COPY src/provisioning.py .
COPY src/config.py .

# Install wait-for utility
RUN wget https://raw.githubusercontent.com/eficode/wait-for/master/wait-for
RUN chmod +x wait-for

CMD ["python", "app.py"]
