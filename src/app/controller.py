import requests
from flask import Blueprint, abort, jsonify, request

from app.models import Product, ProductStock, db
from app.utils import model_to_json
from app.validators import (
    create_custom_product_validator,
    create_product_validator, update_product_validator)
from config import OPEN_FOOD_FACT_URL

api_controller = Blueprint("api_controller", __name__)


@api_controller.route("/product/stock/<int:stock_id>")
def get_products(stock_id):
    res = ProductStock.query.filter_by(stock_id=stock_id).all()
    return model_to_json(res)


@api_controller.route("/product/custom/stock/<int:stock_id>", methods=["POST"])
def create_custom_product(stock_id):
    if not create_custom_product_validator(request):
        abort(400)
    product = Product(name=request.json.get("name"))
    db.session.add(product)
    db.session.commit()
    productStock = ProductStock(stock_id, product._id, request.json.get("quantity"))
    db.session.add(productStock)
    db.session.commit()
    return model_to_json(product), 201


@api_controller.route("/product/stock/<int:stock_id>", methods=["POST"])
def create_product(stock_id):
    if not create_product_validator(request):
        abort(400)
    product = Product.query.get_or_404(request.json.get("product_id"))

    product_exist = ProductStock.query.filter_by(
        product_id=product._id, stock_id=stock_id
    ).first()
    if product_exist is not None:
        abort(409)

    productStock = ProductStock(stock_id, product._id, request.json.get("quantity"))
    db.session.add(productStock)
    db.session.commit()
    return model_to_json(product), 201


@api_controller.route(
    "/product/<int:product_id>/stock/<int:stock_id>", methods=["DELETE"]
)
def delete_product(product_id, stock_id):
    product = ProductStock.query.filter_by(
        product_id=product_id, stock_id=stock_id
    ).first_or_404()
    db.session.delete(product)
    db.session.commit()
    return jsonify({"id": product_id})


@api_controller.route("/product/<int:product_id>/stock/<int:stock_id>", methods=["PUT"])
def update_product(product_id, stock_id):
    if not update_product_validator(request):
        abort(400)
    product = ProductStock.query.filter_by(
        product_id=product_id, stock_id=stock_id
    ).first_or_404()
    product.quantity = request.json.get("quantity")
    db.session.commit()
    return model_to_json(product)


@api_controller.route("/product/barcode/<string:barcode>")
def get_by_barcode(barcode):
    product = Product.query.filter_by(barcode=barcode).first()
    if product:
        return model_to_json(product)
    r = requests.get(OPEN_FOOD_FACT_URL + str(barcode) + ".json").json()
    if int(r.get("status")) == 0:
        abort(404)
    product = Product.from_json(r.get("product"), barcode=barcode)
    if not product:
        abort(404)

    db.session.add(product)
    db.session.commit()

    return model_to_json(product)
