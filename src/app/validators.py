def update_product_validator(request):
    if not request.json:
        return False
    # Must contain a quantity as a number
    if "quantity" not in request.json:
        return False

    try:
        int(request.json.get("quantity"))
        return True
    except ValueError:
        return False


def create_product_validator(request):
    if not request.json:
        return False
    # Must contain a product and quantity as a number
    if "quantity" not in request.json or "product_id" not in request.json:
        return False

    try:
        int(request.json.get("quantity"))
        int(request.json.get("product_id"))
        return True
    except ValueError:
        return False


def create_custom_product_validator(request):
    if not request.json:
        return False
    # Must contain a product name  and quantity as a number
    if "quantity" not in request.json or "name" not in request.json:
        return False

    try:
        int(request.json.get("quantity"))
        return True
    except ValueError:
        return False
