from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship

db = SQLAlchemy()


class ProductStock(db.Model):
    __tablename__ = "product_stock"
    _id = db.Column("id", db.Integer, primary_key=True)
    product_id = db.Column(db.Integer, ForeignKey("product.id"))
    product = relationship("Product")
    stock_id = db.Column("stock_id", db.Integer)
    quantity = db.Column("quantity", db.Integer)

    def __init__(self, stock_id, product_id, quantity):
        self.product_id = product_id
        self.stock_id = stock_id
        self.quantity = quantity

    def serialize(self):
        return {
            "product": self.product.serialize(),
            "stock": self.stock_id,
            "quantity": self.quantity,
        }


class Product(db.Model):
    _id = db.Column("id", db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    barcode = db.Column(db.String(13))
    quantity = db.Column(db.String(10))
    brand = db.Column(db.String(255))
    nutriscore = db.Column(db.String(1))
    image = db.Column(db.String(255))

    def __init__(self, name="", barcode="", quantity="", brand=""):
        self.name = name
        self.barcode = barcode
        self.quantity = quantity
        self.brand = brand

    def serialize(self):
        return {
            "id": self._id,
            "name": self.name,
            "unitary_quantity": self.quantity,
            "brand": self.brand,
            "nutriscore": self.nutriscore,
            "image": self.image,
        }

    def from_json(json, barcode=""):
        product = Product()
        if json.get("product_name_fr"):
            product.name = json.get("product_name_fr")
        elif json.get("product_name_en"):
            product.name = json.get("product_name_en")
        else:
            # A product without name can not be created
            return None
        product.barcode = barcode

        if json.get("brands"):
            product.brand = json.get("brands")
        if json.get("quantity"):
            product.quantity = json.get("quantity")
        if json.get("nutrition_grade_fr"):
            product.nutriscore = json.get("nutrition_grade_fr")
        if json.get("image_url"):
            product.image = json.get("image_url")
        return product
