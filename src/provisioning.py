from app import create_app
from app.models import Product, ProductStock, db


def provisioning(db):
    json1 = {
        "nutrition_grade_fr": "e",
        "brands": "Oasis",
        "product_name_fr": "Tropical Oasis",
        "quantity": "50 cl",
        "image_url": "https://static.openfoodfacts.org/images/products/312/448/016/7057/front_fr.62.400.jpg",
    }
    p1 = Product.from_json(json1, barcode="3124480167057")

    json2 = {
        "brands": "Coca-Cola",
        "nutrition_grade_fr": "e",
        "product_name_fr": "Coca-cola",
        "quantity": "330 ml",
        "image_url": "https://static.openfoodfacts.org/images/products/544/900/021/4911/front_fr.63.400.jpg",
    }
    p2 = Product.from_json(json2, barcode="5449000214911")

    json3 = {
        "quantity": "80 g",
        "product_name_fr": "Gaufre gout chocolat",
        "nutrition_grade_fr": "e",
        "product_name_fr": "Gaufre gout chocolat",
        "image_url": "https://static.openfoodfacts.org/images/products/328/132/202/0809/front_fr.3.400.jpg",
    }
    p3 = Product.from_json(json3, barcode="3281322020809")

    ps1 = ProductStock(stock_id=1, product_id=1, quantity=5)
    ps2 = ProductStock(stock_id=1, product_id=2, quantity=2)
    ps3 = ProductStock(stock_id=1, product_id=3, quantity=4)

    db.session.add(p1)
    db.session.add(p2)
    db.session.add(p3)
    db.session.add(ps1)
    db.session.add(ps2)
    db.session.add(ps3)
    db.session.commit()


if __name__ == "__main__":
    try:
        app = create_app()
        db.app = app
        db.drop_all()
        print("Database successfuly dropped !")
        db.create_all()
        print("Database successfuly created !")
        provisioning(db)
    except Exception as e:
        print(e)
