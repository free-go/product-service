import json
import unittest

from app import create_app
from app.models import db
from config import TEST_DATABASE
from provisioning import provisioning


class BasicTest(unittest.TestCase):
    def setUp(self):
        app = create_app()
        app.config["TESTING"] = True
        app.config["WTF_CSRF_ENABLED"] = False
        app.config["DEBUG"] = False
        app.config["SQLALCHEMY_DATABASE_URI"] = TEST_DATABASE
        self.app = app

        db.init_app(self.app)
        with self.app.app_context():
            db.drop_all()
            db.create_all()
            provisioning(db)
        self.app = app.test_client()

    def tearDown(self):
        pass

    def test_get_all_products(self):
        rv = self.app.get("/product/stock/1")
        assert len(rv.json) == 3
        assert rv.status_code == 200

    def test_create_a_product_success(self):
        data = json.dumps({"product_id": 2, "quantity": 10})
        rv = self.app.post(
            "/product/stock/2", data=data, content_type="application/json"
        )
        assert rv.status_code == 201
        rv = self.app.get("/product/stock/2")
        assert len(rv.json) == 1
        assert rv.status_code == 200
        assert rv.json[0].get("product").get("id") == 2
        assert rv.json[0].get("product").get("name") == "Coca-cola"
        assert rv.json[0].get("quantity") == 10

    def test_create_a_product_invalid(self):
        data = json.dumps({"product_id": "r", "quantity": "r"})
        rv = self.app.post(
            "/product/stock/1", data=data, content_type="application/json"
        )
        assert rv.status_code == 400

    def test_create_a_product_product_already_added(self):
        data = json.dumps({"product_id": "1", "quantity": "1"})
        rv = self.app.post(
            "/product/stock/1", data=data, content_type="application/json"
        )
        assert rv.status_code == 409

    def test_update_a_product_success(self):
        data = json.dumps({"quantity": 666})
        rv = self.app.put(
            "product/1/stock/1", data=data, content_type="application/json"
        )
        assert rv.status_code == 200
        # The product has been changed
        rv = self.app.get("/product/stock/1")
        assert rv.status_code == 200
        assert rv.json[0].get("quantity") == 666

    def test_create_a_custom_product_success(self):
        data = json.dumps({"name": "Pomme rouge", "quantity": 10})
        rv = self.app.post(
            "/product/custom/stock/1", data=data, content_type="application/json"
        )
        assert rv.status_code == 201
        rv = self.app.get("/product/stock/1")
        assert len(rv.json) == 4
        assert rv.status_code == 200
        assert rv.json[3].get("product").get("name") == "Pomme rouge"
        assert rv.json[3].get("quantity") == 10

    def test_create_a_custom_product_invalid(self):
        data = json.dumps({"yolo": "Pomme rouge", "quantity": 10})
        rv = self.app.post(
            "/product/custom/stock/1", data=data, content_type="application/json"
        )
        assert rv.status_code == 400

    def test_update_a_product_no_quantity(self):
        data = json.dumps({"nothing": 666})
        rv = self.app.put(
            "product/1/stock/1", data=data, content_type="application/json"
        )
        assert rv.status_code == 400

    def test_update_a_product_invalid_quantity(self):
        data = json.dumps({"nothing": "random stuff"})
        rv = self.app.put(
            "product/1/stock/1", data=data, content_type="application/json"
        )
        assert rv.status_code == 400

    def test_delete_a_product(self):
        rv = self.app.delete("/product/1/stock/1")
        assert rv.status_code == 200
        assert rv.json == {"id": 1}
        rv = self.app.get("/product/stock/1")
        assert len(rv.json) == 2
        assert rv.status_code == 200

    def test_barcode_success(self):
        rv = self.app.get("/product/barcode/3274080005003")
        assert rv.status_code == 200
        assert b"Eau de source" in rv.data

    def test_barcode_do_not_duplicate(self):
        rv = self.app.get("/product/barcode/3274080005003")
        id1 = rv.json.get("id")
        rv = self.app.get("/product/barcode/3274080005003")
        id2 = rv.json.get("id")
        assert id1 == id2

    def test_barcode_fail(self):
        rv = self.app.get("/product/barcode/666")
        assert rv.status_code == 404
