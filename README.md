# product-service

Micro-service running everything product related.

## Docker

To build the docker & run it manually :

```bash
# In the project root folder
docker build -t product-service .

# Port 5000 is Flask standard
docker run -d --name product-service -p 5000:8080 product-service
```
